#include <stdio.h>
#define MAX 100

int main()
{
    char sen[MAX], ch;
    int count = 0, i;
    printf("Enter the sentence: \n");
    fgets(sen, sizeof(sen), stdin);
    printf("Enter the character: ");
    scanf("%c", &ch);
    for(i = 0; sen[i] != '\0'; i++)
    {
        if(ch == sen[i])
            count++;
    }
    printf("Frequency of %c = %d", ch, count);
    return 0;
}
